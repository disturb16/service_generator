package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/disturb16/creator/codebase"
)

func main() {

	project := flag.String("project-name", "my_service", "Project's name")
	module := flag.String("module", "", "Project's module (example: github.com/foo/bar))")
	flag.Parse()

	if len(*module) < 1 {
		fmt.Println("parameter -module is missing")
		return
	}

	moduleName := strings.Trim(*module, " \n")
	prjectName := strings.Trim(*project, " \n")

	structs := codebase.FilesStruct

	for _, s := range structs {
		//replace module name
		code1 := strings.Replace(s.Code, "$moduleName", moduleName, -1)

		//replace project name
		code := []byte(strings.Replace(code1, "$prjectName", prjectName, -1))

		path := filepath.Join(".", prjectName+s.Path)
		_ = os.MkdirAll(path, 0775)

		err := ioutil.WriteFile(path+"/"+s.Name, code, 0644)

		if err != nil {
			log.Fatal(err.Error())
		}
	}

	log.Println(prjectName + " created successfully.")
}
