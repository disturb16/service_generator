package codebase

type fileString struct {
	Name string
	Path string
	Code string
}

var (
	// FilesStruct ...
	FilesStruct = []fileString{
		//handlers
		fileString{
			Name: "handler.go",
			Path: "/internal/handlers",
			Code: "package handlers\r\n\r\nimport (\r\n\t\"errors\"\r\n\t\"net/http\"\r\n\t\"time\"\r\n\r\n\t\"$moduleName/internal/models\"\r\n\t\"$moduleName/internal/services\"\r\n\t\"$moduleName/logger\"\r\n)\r\n\r\n// handler errors\r\nvar (\r\n\terrAllResorts = errors.New(\"Resorts error: Could not get all resorts\")\r\n)\r\n\r\n// Handler main structure with handlers functionality\r\ntype Handler struct {\r\n\tService *services.Service\r\n}\r\n\r\n// New initialize main handler\r\nfunc New(s *services.Service) *Handler {\r\n\treturn &Handler{\r\n\t\tService: s,\r\n\t}\r\n}\r\n\r\n// Home handler example returning resorts data\r\nfunc (h *Handler) Home(w http.ResponseWriter, req *http.Request) {\r\n\t// getting the context from the request which should have the requestId and apiKey\r\n\tctx := req.Context()\r\n\t// loggin how many milliseconds passed for this function\r\n\tdefer logger.TrackExecutionTime(ctx, \"handlers.Home\", time.Now())\r\n\r\n\t// get resorts data from service object\r\n\tresorts, err := h.Service.GetAllResorts(ctx)\r\n\tif err != nil {\r\n\t\tlogger.LogError(ctx, \"error=\\\"\"+err.Error()+\"\\\"\")\r\n\t\tw.WriteHeader(http.StatusInternalServerError)\r\n\t\tsendJSONError(w, err, errAllResorts.Error())\r\n\t\treturn\r\n\t}\r\n\r\n\t// remove resorts with empty city\r\n\toutput := []models.Resort{}\r\n\tfor _, resort := range resorts {\r\n\t\tif len(resort.City) > 0 {\r\n\t\t\toutput = append(output, resort)\r\n\t\t}\r\n\t}\r\n\r\n\tsendJSONResponse(w, output)\r\n}\r\n",
		},
		fileString{
			Name: "handler_utils.go",
			Path: "/internal/handlers",
			Code: "package handlers\r\n\r\nimport (\r\n\t\"bytes\"\r\n\t\"encoding/json\"\r\n\t\"io\"\r\n\t\"net/http\"\r\n)\r\n\r\ntype intrfc interface{}\r\n\r\n// httpResponse standar json response\r\ntype httpResponse struct {\r\n\tError string `json:\"error,omitempty\"`\r\n\tData  intrfc `json:\"data,omitempty\"`\r\n}\r\n\r\n// jsonConverter converts json data into model\r\nfunc jsonConverter(data io.ReadCloser, model *interface{}) error {\r\n\tdecoder := json.NewDecoder(data)\r\n\terr := decoder.Decode(model)\r\n\treturn err\r\n}\r\n\r\n// sendJSONResponse sends standar json response\r\nfunc sendJSONResponse(w http.ResponseWriter, data interface{}) {\r\n\tresponse := &httpResponse{\r\n\t\tData: data,\r\n\t}\r\n\tw.Header().Set(\"Content-Type\", \"application/json\")\r\n\tjson.NewEncoder(w).Encode(response)\r\n}\r\n\r\n// sendJSONError sends json response with specified error\r\nfunc sendJSONError(w http.ResponseWriter, err error, data interface{}) {\r\n\tresponse := &httpResponse{\r\n\t\tError: err.Error(),\r\n\t\tData:  data,\r\n\t}\r\n\tw.Header().Set(\"Content-Type\", \"application/json\")\r\n\tjson.NewEncoder(w).Encode(response)\r\n}\r\n\r\n// httpRequest creates request struct\r\ntype httpRequest struct {\r\n\tResponse *http.Response\r\n\tURL      string\r\n\tData     []byte\r\n\tMethod   string\r\n\trequest  *http.Request\r\n\tHeaders  map[string]string\r\n}\r\n\r\n// Send Sends http request\r\nfunc (r *httpRequest) Send() error {\r\n\thttpClient := http.Client{}\r\n\r\n\tvar err error\r\n\r\n\tr.request, err = http.NewRequest(r.Method, r.URL, bytes.NewBuffer(r.Data))\r\n\tif err != nil {\r\n\t\treturn err\r\n\t}\r\n\r\n\t// Add headers to the request\r\n\tfor index, header := range r.Headers {\r\n\t\tr.request.Header.Add(index, header)\r\n\t}\r\n\r\n\t// make requests and store response\r\n\tr.Response, err = httpClient.Do(r.request)\r\n\r\n\treturn err\r\n}\r\n",
		},
		fileString{
			Name: "routes.go",
			Path: "/internal/handlers",
			Code: "package handlers\r\n\r\nimport (\r\n\t\"context\"\r\n\t\"net/http\"\r\n\r\n\t\"github.com/gorilla/mux\"\r\n\t\"$moduleName/settings\"\r\n)\r\n\r\n// Middleware that sets requestId and apiKey variables in context\r\nfunc initBaseContext(next http.Handler) http.Handler {\r\n\treturn http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {\r\n\r\n\t\trequestID := req.Header.Get(\"Kong-Request-ID\")\r\n\t\tapiKey := req.Header.Get(\"api-key\")\r\n\r\n\t\treqTrack := map[string]string{\r\n\t\t\t\"Kong-Request-ID\": requestID,\r\n\t\t\t\"api-key\":         apiKey,\r\n\t\t}\r\n\r\n\t\tctx := context.WithValue(req.Context(), settings.RequestTracking, reqTrack)\r\n\t\treq = req.WithContext(ctx)\r\n\r\n\t\t//call hanlder\r\n\t\tnext.ServeHTTP(w, req)\r\n\t})\r\n}\r\n\r\n// Router returns api router\r\nfunc (h *Handler) Router() *mux.Router {\r\n\trouter := mux.NewRouter()\r\n\r\n\t// set default prefix for Service\r\n\tapi := router.PathPrefix(\"/service/my-service/api/v1\").Subrouter()\r\n\r\n\t// set endpoints\r\n\tapi.HandleFunc(\"/home\", h.Home).Methods(\"GET\")\r\n\r\n\t// sets base context data for all handlers\r\n\tapi.Use(initBaseContext)\r\n\r\n\treturn router\r\n}\r\n",
		},

		//model
		fileString{
			Name: "resort.go",
			Path: "/internal/models",
			Code: "package models\r\n\r\n// Resort example of structure for resorts\r\ntype Resort struct {\r\n\tID   *int64 `json:\"resort_id\"`\r\n\tName string `json:\"resort_name\"`\r\n\tCode string `json:\"resort_code\"`\r\n\tCity string `json:\"resort_city\"`\r\n}\r\n",
		},

		// service
		fileString{
			Name: "service.go",
			Path: "/internal/services",
			Code: "package services\r\n\r\nimport (\r\n\t\"context\"\r\n\t\"database/sql\"\r\n\t\"errors\"\r\n\t\"fmt\"\r\n\t\"time\"\r\n\r\n\t\"$moduleName/logger\"\r\n\r\n\t_ \"github.com/go-sql-driver/mysql\" // mysql driver\r\n\t\"$moduleName/internal/models\"\r\n\t\"$moduleName/settings\"\r\n)\r\n\r\n// Service errors\r\nvar (\r\n\tErrTest = errors.New(\"Error on service\")\r\n)\r\n\r\n// Service struct definition containing all services functionalties\r\ntype Service struct {\r\n\tDB *sql.DB\r\n}\r\n\r\n// New initialize service and database connections\r\nfunc New(dbDriver string, config *settings.Configuration) (*Service, error) {\r\n\r\n\ts := &Service{}\r\n\r\n\t// setup database connection\r\n\tconnectionString := fmt.Sprintf(\r\n\t\t\"%s:%s@tcp(%s:%s)/%s\",\r\n\t\tconfig.DB.User,\r\n\t\tconfig.DB.Password,\r\n\t\tconfig.DB.Host,\r\n\t\tconfig.DB.Port,\r\n\t\tconfig.DB.Name)\r\n\r\n\tdb, err := sql.Open(dbDriver, connectionString)\r\n\r\n\tif err != nil {\r\n\t\treturn nil, err\r\n\t}\r\n\r\n\ts.DB = db\r\n\r\n\treturn s, nil\r\n}\r\n\r\n// GetAllResorts service example that returns all resorts from database\r\nfunc (s *Service) GetAllResorts(ctx context.Context) ([]models.Resort, error) {\r\n\tdefer logger.TrackExecutionTime(ctx, \"services.GetAllResorts\", time.Now())\r\n\r\n\tresorts := []models.Resort{}\r\n\r\n\tqry := fmt.Sprintf(`\r\n\t\tSELECT\r\n\t\t\tRESORT_ID,\r\n\t\t\tRESORT_NAME,\r\n\t\t\tRESORT_CODE,\r\n\t\t\tifnull(RESORT_CITY, '')\r\n\t\tFROM RESORTS;\r\n\t`)\r\n\r\n\tif s.DB == nil {\r\n\t\treturn nil, errors.New(\"No database initialized\")\r\n\t}\r\n\r\n\trows, err := s.DB.Query(qry)\r\n\r\n\tif err != nil {\r\n\t\tlogger.LogError(nil, \"Could not get records from table\")\r\n\t\treturn resorts, err\r\n\t}\r\n\tdefer rows.Close()\r\n\r\n\tfor rows.Next() {\r\n\t\trow := models.Resort{}\r\n\t\terr = rows.Scan(&row.ID, &row.Name, &row.Code, &row.City)\r\n\r\n\t\tif err == sql.ErrNoRows {\r\n\t\t\treturn nil, err\r\n\t\t}\r\n\r\n\t\tif err != nil {\r\n\t\t\treturn nil, err\r\n\t\t}\r\n\r\n\t\tresorts = append(resorts, row)\r\n\t}\r\n\r\n\treturn resorts, err\r\n}\r\n",
		},

		//logger
		fileString{
			Name: "logger.go",
			Path: "/logger",
			Code: "package logger\r\n\r\nimport (\r\n\t\"context\"\r\n\t\"fmt\"\r\n\t\"log\"\r\n\t\"path/filepath\"\r\n\t\"runtime\"\r\n\t\"time\"\r\n\r\n\t\"$moduleName/settings\"\r\n)\r\n\r\n// LogError Prints error and line where error occurred\r\nfunc LogError(ctx context.Context, msg string) {\r\n\r\n\tvar errToDisplay string\r\n\t_, file, _, _ := runtime.Caller(1)\r\n\tlog.Printf(\"\\033[31m Error on %s: \\033[39m\", append([]interface{}{filepath.Base(file)})...)\r\n\r\n\t// validate if context was passed\r\n\tif ctx != nil {\r\n\r\n\t\t// get variables to track request\r\n\t\tval := ctx.Value(settings.RequestTracking)\r\n\r\n\t\t//assert data is a map of strings\r\n\t\tdata, ok := val.(map[string]string)\r\n\r\n\t\tif ok {\r\n\t\t\tapikey := data[\"api-key\"]\r\n\t\t\trequestID := data[\"Kong-Request-ID\"]\r\n\t\t\terrToDisplay = \"\\trequestId=\\\"%s\\\", apiKey=\\\"%s', %s\\n\"\r\n\t\t\tfmt.Printf(errToDisplay, requestID, apikey, msg)\r\n\t\t} else {\r\n\t\t\tfmt.Printf(\"\\t %s\\n\", msg)\r\n\t\t}\r\n\r\n\t} else {\r\n\t\tfmt.Printf(\"\\t %s\\n\", msg)\r\n\t}\r\n\r\n}\r\n\r\n// TrackExecutionTime logs time elapsed for caller function\r\nfunc TrackExecutionTime(ctx context.Context, caller string, t time.Time) {\r\n\telapsed := time.Since(t)\r\n\r\n\tif caller == \"\" {\r\n\t\tfmt.Println(\"TrackExecutionTime Caller is nil...\")\r\n\t}\r\n\r\n\tif ctx == nil {\r\n\t\tlog.Println(fmt.Sprintf(\"Function=\\\"%s\\\" ms=\\\"%f\\\"\", caller, float64(elapsed.Nanoseconds())/float64(time.Millisecond)))\r\n\t} else {\r\n\t\tmsg := fmt.Sprintf(\"Function=\\\"%s\\\" ms=\\\"%f\\\"\", caller, float64(elapsed.Nanoseconds())/float64(time.Millisecond))\r\n\t\tLog(ctx, msg)\r\n\t}\r\n}\r\n\r\n//Log logs standard message\r\nfunc Log(ctx context.Context, msg string) {\r\n\r\n\tif ctx == nil {\r\n\t\tlog.Println(msg + \"\\n\")\r\n\t}\r\n\r\n\t// get variables to track request\r\n\tval := ctx.Value(settings.RequestTracking)\r\n\r\n\t//assert data is a map of strings\r\n\tdata, ok := val.(map[string]string)\r\n\r\n\tif ok {\r\n\t\tapikey := data[\"api-key\"]\r\n\t\trequestID := data[\"Kong-Request-ID\"]\r\n\t\tlog.Printf(\"requestId=\\\"%s\\\", apiKey=\\\"%s\\\", %s\\n\", requestID, apikey, msg)\r\n\t} else {\r\n\t\tLogError(nil, \"Invalid request-tracking value\")\r\n\t\tlog.Println(msg + \"\\n\")\r\n\t}\r\n}\r\n",
		},

		// settings
		fileString{
			Name: "configuration.go",
			Path: "/settings",
			Code: "package settings\r\n\r\nimport (\r\n\t\"io/ioutil\"\r\n\t\"log\"\r\n\t\"os\"\r\n\r\n\t\"gopkg.in/yaml.v2\"\r\n)\r\n\r\n//Configuration main configuration params\r\ntype Configuration struct {\r\n\tPort   string `yaml:\"Port\"`\r\n\tAPIKey string `yaml:\"ApiKey\"`\r\n\tDB     struct {\r\n\t\tHost     string `yaml:\"Host\"`\r\n\t\tName     string `yaml:\"Name\"`\r\n\t\tPort     string `yaml:\"Port\"`\r\n\t\tUser     string `yaml:\"User\"`\r\n\t\tPassword string `yaml:\"Password\"`\r\n\t} `yaml:\"Database\"`\r\n}\r\n\r\n// GetEnv returns env value, if empty, returns fallback value\r\nfunc GetEnv(key string, fallback string) string {\r\n\tvalue, ok := os.LookupEnv(key)\r\n\tif !ok {\r\n\t\treturn fallback\r\n\t}\r\n\r\n\treturn value\r\n}\r\n\r\n//GetConfiguration returns main configuration object\r\nfunc GetConfiguration() (*Configuration, error) {\r\n\r\n\tconfigYaml := &Configuration{}\r\n\tvar err error\r\n\tvar confFile []byte\r\n\r\n\t// check if conf-file is .yaml or .json\r\n\tconfFile, err = ioutil.ReadFile(\"conf.yaml\")\r\n\tif err != nil {\r\n\t\tconfFile, err = ioutil.ReadFile(\"conf.json\")\r\n\t\tif err != nil {\r\n\t\t\tlog.Println(\"Could not open conf.yaml or conf.json\")\r\n\t\t}\r\n\t}\r\n\r\n\t//if file exists use its variables\r\n\tif err == nil {\r\n\t\terr = yaml.Unmarshal(confFile, &configYaml)\r\n\t\tif err != nil {\r\n\t\t\treturn nil, err\r\n\t\t}\r\n\t}\r\n\r\n\t//Search for OS environment and use conf-file as fallback\r\n\tconfig := &Configuration{}\r\n\tconfig.Port = GetEnv(\"PORT\", configYaml.Port)\r\n\tconfig.APIKey = GetEnv(\"APIKEY\", configYaml.APIKey)\r\n\tconfig.DB.Name = GetEnv(\"DB_NAME\", configYaml.DB.Name)\r\n\tconfig.DB.Host = GetEnv(\"DB_HOST\", configYaml.DB.Host)\r\n\tconfig.DB.User = GetEnv(\"DB_USER\", configYaml.DB.User)\r\n\tconfig.DB.Password = GetEnv(\"DB_PASS\", configYaml.DB.Password)\r\n\tconfig.DB.Port = GetEnv(\"DB_PORT\", configYaml.DB.Port)\r\n\r\n\treturn config, nil\r\n}\r\n",
		},

		fileString{
			Name: "constants.go",
			Path: "/settings",
			Code: "package settings\r\n\r\ntype contextCey string\r\n\r\nconst (\r\n\t//RequestTracking is a map containing\r\n\t// all variables related to kong gateway\r\n\t// Kong-Request-ID, api-key\r\n\tRequestTracking = \"request-tracking\"\r\n)\r\n",
		},

		// main
		fileString{
			Name: "main.go",
			Path: "/",
			Code: "package main\r\n\r\nimport (\r\n\t\"fmt\"\r\n\t\"log\"\r\n\t\"net/http\"\r\n\r\n\t\"$moduleName/internal/handlers\"\r\n\t\"$moduleName/internal/services\"\r\n\t\"$moduleName/settings\"\r\n)\r\n\r\nfunc main() {\r\n\t//Get project configurations\r\n\tconfig, err := settings.GetConfiguration()\r\n\tif err != nil {\r\n\t\tlog.Fatal(err)\r\n\t}\r\n\r\n\tif config.Port == \"\" {\r\n\t\tlog.Fatal(\"No port was given\")\r\n\t}\r\n\r\n\t// initialize service\r\n\tservice, err := services.New(\"mysql\", config)\r\n\tif err != nil {\r\n\t\tlog.Fatalf(\"Could not start service... %v\", err)\r\n\t}\r\n\r\n\t//Create main handler\r\n\thandler := handlers.New(service)\r\n\r\n\t//Start server\r\n\tfmt.Println(\"server running on port \" + config.Port)\r\n\tlog.Fatal(http.ListenAndServe(\":\"+config.Port, handler.Router()))\r\n}\r\n",
		},
		fileString{
			Name: "go.mod",
			Path: "/",
			Code: "module $moduleName",
		},
		fileString{
			Name: "Dockerfile",
			Path: "/",
			Code: "#BUILD STAGE\r\nFROM golang:alpine AS builder\r\n\r\n#add label to be able to filter temporary images\r\nLABEL stage=builder\r\n\r\n#set working directory outside gopath\r\nWORKDIR /$prjectName\r\nCOPY . .\r\nRUN apk add --no-cache git && \\\r\n    go get -d -v ./...\r\n\r\nRUN go build -o ./$prjectName\r\n\r\n# --------------------------------------------------\r\n\r\n#FINAL STAGE\r\nFROM alpine:latest\r\nRUN apk --no-cache add ca-certificates\r\nWORKDIR /$prjectName\r\n\r\n#copy the whole project folder from temporary image and set entrypoint\r\nCOPY --from=builder /$prjectName .\r\nENTRYPOINT ./$prjectName\r\nLABEL Name=$prjectName Version=0.1\r\nEXPOSE 5001\r\n\r\n\r\n## build image and remove temporary images:\r\n#docker build -t $prjectName . && docker image prune --filter label=stage=builder -f",
		},
		fileString{
			Name: ".dockerignore",
			Path: "/",
			Code: "node_modules\r\nnpm-debug.log\r\nDockerfile*\r\ndocker-compose*\r\n.dockerignore\r\n.git\r\n.gitignore\r\n.env\r\n*/bin\r\n*/obj\r\nREADME.md\r\nLICENSE\r\n.vscode",
		},
		fileString{
			Name: ".gitignore",
			Path: "/",
			Code: ".vscode\r\n.DS_store\r\n.env\r\nconf.yaml\r\nconf.json",
		},
		fileString{
			Name: "conf.yaml",
			Path: "/",
			Code: "Port: 8080\r\nDatabase:\r\n  Port: 3306\r\n  Name: \"\"\r\n  User: \"\"\r\n  Password: \"\"\r\n  Host: \"\"",
		},
	}
)
