module github.com/disturb16/creator

go 1.12

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.0
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
